import Vue from 'vue'
import Router from 'vue-router'
import Tractors from '@/pages/tractors'
import Sessions from "@/pages/sessions";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Tractors
    },
    {
      path: '/tractors',
      name: 'tractors',
      component: Tractors
    },
    {
      path: '/sessions/:tractor?/:session?/:sensor?',
      name: 'sessions',
      component: Sessions,
      props: (route) => ({ tractor: route.params.tractor })
    }

  ]
})
