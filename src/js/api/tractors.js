async function fetchTractors() {
  return (await _fetch(`${window.env.API}/tractors`)).json();
}

async function fetchSessions(tractor) {
  return (await _fetch(`${window.env.API}/tractors/${tractor}/sessions`)).json();
}

async function fetchSensors(tractor, session) {
  return (await _fetch(`${window.env.API}/tractors/${tractor}/sessions/${session}/sensors`)).json();
}

async function fetchSensor(tractor, session, sensor, resolution) {
  return (await _fetch(`${window.env.API}/tractors/${tractor}/sessions/${session}/sensors/${sensor}?resolution=${resolution}`)).json();
}

async function _fetch(url) {
  let response = await fetch(url);
  await handleError(response);
  return response;
}

async function handleError(response) {
  if (response.ok) {
    return;
  }

  let result = await response.json();
  if (result.error) {
    throw Error(`API reports: ${result.error}`)
  }

  throw Error(response.reason)
}

export default {fetchTractors, fetchSessions , fetchSensors, fetchSensor}
