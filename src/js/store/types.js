import * as geo from "d3-geo";
import {extent} from "d3";

export function FeatureCollection(points) {
  return {
    extent() {
      return extent(this.features, f => f.properties.val);
    },
    bounds() {
      let bounds = geo.geoBounds(this);
      return [
        [bounds[0][1], bounds[0][0]],
        [bounds[1][1], bounds[1][0]],
      ];
    },
    getFeatureAt(latlng, range) {
      let s_dist = 1.0, closest = null;
      for (let feat of this.features) {
        let dist = geo.geoDistance([latlng.lng, latlng.lat], feat.geometry.coordinates);
        if (dist < s_dist) {
          s_dist = dist;
          closest = feat;
        }
      }
      if (s_dist > range) {
        return null
      }

      return closest;
    },
    "type": "FeatureCollection",
    "features": points.map(p => {
      return {
        "type": "Feature",
        "geometry": {
          "type": "Point",
          "coordinates": [p.lon, p.lat]
        },
        "properties": {
          "ts": p.ts,
          "val": p.value,
          "spd": p.speed,
          "trk": p.track
        }
      }
    }),
  }
}
