import api from '../../api/tractors'
import {FeatureCollection} from "../types";
import * as d3 from "d3";

const ALPHA=0.8;

const state = {
  sensor: null,
  loading: false,
  extent: null,
  scale: null,
  selected: null,
  color_scheme: [
    `rgb(128,0,38,${ALPHA})`,
    `rgb(189,0,38,${ALPHA})`,
    `rgb(227,26,28,${ALPHA})`,
    `rgb(252,78,42,${ALPHA})`,
    `rgb(253,141,60,${ALPHA})`,
    `rgb(254,178,76,${ALPHA})`,
    `rgb(254,217,118,${ALPHA})`,
    `rgb(255,237,160,${ALPHA})`,
    `rgb(255,255,204,${ALPHA})`,
  ]
};

const actions = {
  async loadSensorData(store, {tractor, session, sensor}) {
    store.commit('setLoading', true);
    let sensorData = await api.fetchSensor(tractor, session, sensor, 10);
    store.commit('setSensorData', sensorData);
    store.dispatch('setScaleExtent', state.sensor.data.readings.extent());
    store.commit('setLoading', false);
    setTimeout(async () => {
      store.commit('setLoading', true);
      sensorData = await api.fetchSensor(tractor, session, sensor, 1);
      store.commit('setSensorData', sensorData);
      store.dispatch('setScaleExtent', sensorData.data.readings.extent());
      store.commit('setLoading', false);
    }, 1000);
  },
  async setScaleExtent(store, extent) {
    store.commit('updateScale', extent);
  },
  async selectFeatureAt(store, {latlng,range}) {
    store.commit('setSelectedFeature', state.sensor.data.readings.getFeatureAt(latlng, range))
  }
};

const mutations = {
  clearData(state) {
    state.sensor = null;
    state.loading = false;
    state.extent = null;
    state.scale = null;
    state.selected = null;
  },
  setLoading(state, value) {
    state.loading = value
  },
  setSensorData(state, sensor) {
    // freeze for performance, stops vue from observing array items
    sensor.data.readings = Object.freeze(FeatureCollection(sensor.data.readings));
    state.sensor = sensor;
    state.selected = null;
  },
  updateScale(state, extent) {
    state.extent = extent;
    state.scale =
      d3.scaleQuantize()
        .domain(extent)
        .range(d3.range(state.color_scheme.length));
  },
  setSelectedFeature(state, feature) {
    state.selected = feature
  }
};

const getters = {
  isLoading: state => state.loading,
  isEmpty: state => state.sensor ? state.sensor.data.readings.features.length === 0 : false,
  isDataReady: state => state.sensor !== null && state.loading === false && state.sensor.data.readings.features.length > 0,
  featureCollection: state => state.sensor ? state.sensor.data.readings : null,
  unit: state => state.sensor ? state.sensor.data.unit : null,
  colorScheme: state => state.color_scheme,
  extent: state => state.extent,
  scale: state => state.scale,
  getFeatureAt: state => latlng => {
    return state.sensor.data.readings.getFeatureAt(latlng)
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

