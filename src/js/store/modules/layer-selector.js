import api from '../../api/tractors'

const state = {
  selected: {
    tractor: null,
    session: null,
    sensor: null
  },
  tractors: [],
  sessions: [],
  sensors: [],
};


const actions = {
  async init(store) {
    let tractors = await api.fetchTractors();
    store.commit('setTractors', tractors.data);
  },
  async selectMultiple(store, {tractor, session, sensor}) {
    if (tractor !== undefined) {
      await store.dispatch('selectTractor', tractor)
    }
    if (session !== undefined) {
      await store.dispatch('selectSessionOnly', session)
    }
    if (sensor !== undefined) {
      await store.dispatch('selectSensor', sensor)
    }
  },
  async selectTractor(store, tractor) {
    store.commit('clearSessions');
    store.commit('clearSensors');
    store.commit('selectTractor', tractor);
    let sessions = await api.fetchSessions(state.selected.tractor);
    store.commit('setSessions', sessions.data);
  },
  async selectSession(store, session) {
    store.commit('clearSensors');
    store.commit('selectSession', session);
    let sensors = await api.fetchSensors(state.selected.tractor, state.selected.session);
    store.commit('setSensors', sensors.data);
    store.dispatch('selectSensor', 0)
  },
  async selectSessionOnly(store, session) {
    store.commit('clearSensors');
    store.commit('selectSession', session);
    let sensors = await api.fetchSensors(state.selected.tractor, state.selected.session);
    store.commit('setSensors', sensors.data);
  },
  async selectSensor(store, sensor) {
    store.commit('selectSensor', sensor);
  },
};

const mutations = {
  clearSessions(state) {
    state.sessions = null;
    state.selected.session = null;
  },
  clearSensors(state) {
    state.sensors = null;
    state.selected.sensor = null;
  },
  selectTractor(state, tractor) {
    state.selected.tractor = tractor
  },
  selectSession(state, session) {
    state.selected.session = session;
  },
  selectSensor(state, sensor) {
    state.selected.sensor = sensor;
  },
  setTractors(state, tractors) {
    state.tractors = tractors
  },
  setSessions(state, sessions) {
    state.sessions = sessions;
  },
  setSensors(state, sensors) {
    state.sensors = sensors;
  },
};

const getters = {
  getSensorById: state => sensorId => state.sensors.find(s => s.id == sensorId),
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

