import Vue from 'vue'
import Vuex from 'vuex'
import LayerSelector from './modules/layer-selector'
import LayerData from './modules/layer-data'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    LayerSelector,
    LayerData,
  },
  strict: debug,
})
