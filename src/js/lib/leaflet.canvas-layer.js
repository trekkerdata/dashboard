import * as L from "leaflet";

const MAX_POINTS_WHILE_MOVING = 300;
const MIN_DIST_BETWEEN_POINTS = 5;

export default (featureCollection, renderer) => {
  return new (L.CanvasLayer.extend(
    {
      isMoving: false,
      visiblePoints: [],
      clear() {
        this._ctx.clearRect(-1000, -1000, this._canvas.width + 2000, this._canvas.height + 2000);
      },
      determineVisiblePoints() {
        let points = [];
        let prevPoint = null;
        let size = this._map.getSize();
        let everySoMany = Math.trunc(featureCollection().features.length / MAX_POINTS_WHILE_MOVING);
        let n = 0;
        for (let point of featureCollection().features) {
          // skip every soo many points while layer is moving
          if (this.isMoving) {
            if (n++ % everySoMany !== 0) continue;
          }

          let pixelPoint = this.project(point);

          // skip if point does not lie within visible area
          if (pixelPoint.x < 0 || pixelPoint.y < 0 || pixelPoint.x > size.x || pixelPoint.y > size.y) {
            continue;
          }

          // skip if point is too close to previous point
          if (prevPoint != null && this.distance(pixelPoint, prevPoint) < MIN_DIST_BETWEEN_POINTS) {
            continue;
          }
          prevPoint = pixelPoint;

          points.push(pixelPoint);
        }

        return points;
      },
      render() {
        if (this.visiblePoints.length === 0) {
          this.visiblePoints = this.determineVisiblePoints();
        }

        this.clear();
        renderer(this._ctx, this.visiblePoints);

        if (this.isMoving) {
          this.visiblePoints = []
        }
      },
      distance(p1, p2) {
        let a2 = Math.abs(p1.x - p2.x) ** 2;
        let b2 = Math.abs(p1.y - p2.y) ** 2;
        return Math.sqrt(a2 + b2);
      },
      project(point) {
        let projected = this._map.latLngToContainerPoint(
          [point.geometry.coordinates[1], point.geometry.coordinates[0]]
        );
        return {
          ...projected,
          value: point.properties.val,
          trk: point.properties.trk,
        }
      },
      getBounds() {
        return featureCollection().bounds()
      },
      onAdd(map) {
        L.CanvasLayer.prototype.onAdd.call(this, map);

        map.on('movestart', this.onMoveStart, this);
        map.on('moveend', this.onMoveEnd, this);

        let canvas = this._canvas;
        let dpi = window.devicePixelRatio;
        let style_width = +getComputedStyle(canvas).getPropertyValue("width").slice(0, -2);
        let style_height = +getComputedStyle(canvas).getPropertyValue("height").slice(0, -2);
        canvas.setAttribute('width', style_width * dpi);
        canvas.setAttribute('height', style_height * dpi);
      },
      onMoveStart() {
        this.isMoving = true;
      },
      onMoveEnd() {
        this.isMoving = false;
        this.render()
      },
      onRemove: function (map) {
        map.off('movestart', this.onMoveStart, this);
        map.off('moveend', this.onMoveEnd, this);
        map.off('move'); // workaround for wrong deregistration of move listener in parent library, do not remove

        L.CanvasLayer.prototype.onRemove.call(this, map);

        this.visiblePoints = null;
      },
    }
  ))();
}
