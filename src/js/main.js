import Vue from 'vue'
import App from './app'
import store from './store'
import router from './router'
import 'materialize-css/sass/materialize.scss';
import 'materialize-css/dist/js/materialize.min';

if (window.env.API === '__API__') {
  window.env.API = 'http://localhost:3000/v3'
}

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  router: router,
  components: {
    app: App,
  },
  template: '<app/>'
});
