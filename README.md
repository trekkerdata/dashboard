## Web viewer for Tractor data.

Web based dashboard app visualising ISOBlue data.

## Local development
```bash
$ npm install
$ API=... npm run hot
```

## Running in production

### Required environment variables

| VAR     | Description | 
|---------|-------------| 
| API     | API endpoint prefix, eg `http://192.168.0.1:8080/v1` |

As long as you set these variables the docker image can be run anywhere you like.

### Deploy
Use `deployment/.env.dist` as a reference for the environment variables to set, and copy to `deployment/.env` 
