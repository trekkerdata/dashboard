#!/usr/bin/env sh

echo "Preparing"

if [[ -z "API" ]]; then
    echo "No API base URI set"
    exit 1
fi

sed -i -e "s/__API__/${API}/" /var/www/html/index.html

exec "$@"
